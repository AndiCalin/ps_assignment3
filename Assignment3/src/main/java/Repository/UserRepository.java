package Repository;

import Entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

import java.util.List;

public class UserRepository{

    @Query("SELECT * FROM users")
    List<User> findAll();



}
